﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MT.BO;
using MT.Model;

namespace MT.Dll
{
    public static class StudentDll
    {
        public static bool addStudent()
        {
            var entities = new StudentEntities();

            var student = new Student();
            student.name = "Bhavik Garg";
            student.email = "bhavik.garg@geminisolutions.in";

            entities.Students.Add(student);
            entities.SaveChanges();
            return true;
        }

        public static List<StudentBO> selectStudents()
        {
            var entities = new StudentEntities();

            var studentList = entities.Students.Select(
                    a => new StudentBO
                    {
                        Name = a.name,
                        Email = a.email
                    }
                   ).ToList();

            return studentList;
        }

        public static StudentBO getStudentByID()
        {
            var entities = new StudentEntities();

            var student = entities.Students
                .Where(a => a.stud_id == 1)
                .Select(
                    a => new StudentBO
                    {
                        Name = a.name,
                        Email = a.email
                    }
                   ).FirstOrDefault();

            return student;
        }

        public static List<StudentBO> FilterStudents()
        {
            var entities = new StudentEntities();

            var studentList = entities.Students
                .Where(a => a.stud_id > 1)
                .Select(
                    a => new StudentBO
                    {
                        Name = a.name,
                        Email = a.email
                    }
                   ).ToList();

            return studentList;
        }

        public static bool UpdateStudent(StudentDetails studDetail)
        {
            var entities = new StudentEntities();
            var studentDetails = entities.Students.FirstOrDefault(a => a.stud_id == studDetail.studId);
            if (studentDetails != null)
            {
                studentDetails.name = studDetail.Name;
                studentDetails.email = studDetail.Email;
                entities.SaveChanges();
                return true;
            }
            return false;
        }

        public static bool DeleteStudentById(StudentDetails studDetail)
        {
            // using is used for Auto Dispose of object entities
            using (var entities = new StudentEntities())
            {
                var studList = entities.Students.FirstOrDefault(a => a.stud_id == studDetail.studId);
                if (studList != null)
                {
                    // soft delete
                    studList.isActive = false;
                    studList.dateDeleted = DateTime.UtcNow;
                    entities.SaveChanges();
                    return true;
                }
                return false;
            }
            
        }
    }
}
