﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MT.Dll;
using MT.BO;
namespace MT.Bll
{
    public static class AddStudent
    {
        public static bool addStudent()
        {
            return StudentDll.addStudent();
        }

        public static List<StudentBO> selectStudents()
        {
            return StudentDll.selectStudents();
        }

        public static StudentBO getStudentByID()
        {
            return StudentDll.getStudentByID();
        }

        public static List<StudentBO> FilterStudents()
        {
            return StudentDll.FilterStudents();
        }

        public static bool UpdateStudent(StudentDetails studentDetail)
        {
            return StudentDll.UpdateStudent(studentDetail);
        }

        public static bool DeleteStudentById(StudentDetails studentDetails)
        {
            return StudentDll.DeleteStudentById(studentDetails);
        }
    }
}
