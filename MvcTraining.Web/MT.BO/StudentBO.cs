﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MT.BO
{
    public class StudentBO
    {
        public string Name { get; set; }
        public string Email { get; set; }
    }

    public class StudentDetails : StudentBO
    {
        public int studId { get; set; }
    }
}
