﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MT.Bll;
using MT.BO;

namespace MvcTraining.Web.Controllers
{
    
    public class StudentController : ApiController
    {
        public bool addStudent()
        {
            return AddStudent.addStudent();
        }

        [HttpGet]
        public List<StudentBO> selectStudents()
        {
            return AddStudent.selectStudents();
        }

        [HttpGet]
        public StudentBO getStudentByID()
        {
            return AddStudent.getStudentByID();
        }

        [HttpGet]
        public List<StudentBO> FilterStudents()
        {
            return AddStudent.FilterStudents();
        }

        [HttpPost]
        public bool UpdateStudent(StudentDetails studentDetail)
        {
            return AddStudent.UpdateStudent(studentDetail);
        }

        [HttpPost]
        public bool DeleteStudentById(StudentDetails studentDetails)
        {
            return AddStudent.DeleteStudentById(studentDetails);
        }

    }
}