﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MT.Dll;
using MT.Bo;

namespace MT.Bll
{
    public class AdminBll
    {
        public static bool AddEmployee(EmployeeBasicInfo emp)
        {
            return AdminDll.AddEmployee(emp);
        }
        public static List<EmployeeListInfo> EmployeeList()
        {
            return AdminDll.EmployeeList();
        }
        public static List<EmployeeListInfo> EmployeeSearch(EmployeeFilter emp)
        {
            return AdminDll.EmployeeSearch(emp);
        }
        public static bool ProfileUpdate(EmployeeProfileUpdate emp)
        {
            return AdminDll.ProfileUpdate(emp);
        }
        public static bool EmployeeUpdate(EmployeeUpdate emp)
        {
            return AdminDll.EmployeeUpdate(emp);
        }
        public static bool EmployeeDelete(EmployeeDelete emp)
        {
            return AdminDll.EmployeeDelete(emp);
        }
        public static bool EmployeeRemove(EmployeeeRemove emp)
        {
            return AdminDll.EmployeeRemove(emp);
        }
        public static EmployeeProfile MyProfile(EmployeeProfile emp)
        {
            return AdminDll.MyProfile(emp);
        }
        public static EmployeeProfile Login(Credential emp)
        {
            return AdminDll.Login(emp);
        }
    }
}
