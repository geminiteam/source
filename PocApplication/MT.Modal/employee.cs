//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MT.Modal
{
    using System;
    using System.Collections.Generic;
    
    public partial class employee
    {
        public long id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Passsword { get; set; }
        public string Mobile { get; set; }
        public System.DateTime DateCreated { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> DateDeleted { get; set; }
        public System.DateTime DOB { get; set; }
        public Nullable<System.DateTime> DateDeactivated { get; set; }
    }
}
