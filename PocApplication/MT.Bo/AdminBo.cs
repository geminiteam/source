﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MT.Modal;

namespace MT.Bo
{
    //Add Employee
    public class EmployeeBasicInfo
    {       
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public System.DateTime DOB { get; set; }       
        public Nullable<bool> IsActive { get; set; }       
        public string Passsword { get; set; }
        public string Mobile { get; set; }
    }
    //Employee Basic Infomation List
    public class EmployeeListInfo
    {
        public long id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public System.DateTime DOB { get; set; }
        public Nullable<System.DateTime> DateCreated { get; set; }
        public Nullable<System.DateTime> DateDeleted { get; set; }
        public Nullable<System.DateTime> DateDeactivated { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }      
        public string Mobile { get; set; }
    }
    //Employee Basic Filter
    public class EmployeeFilter
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
    }
    public class Credential
    {
        public long id { get; set; }
        public string Email { get; set; }
        public string Passsword { get; set; }

    }
    //Employee Details Update
    public class EmployeeUpdate
    {
        public long id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }      
        public System.DateTime DOB { get; set; }
        public string Mobile { get; set; }
    }
    //Employee Profile Update
    public class EmployeeProfileUpdate
    {
        public long id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public System.DateTime DOB { get; set; }
        public string Email { get; set; }
        public string Passsword { get; set; }
        public string Mobile { get; set; }
    }
    //Employee Profile
    public class EmployeeProfile
    {
        public long id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public System.DateTime DOB { get; set; }
        public Nullable<System.DateTime> DateCreated { get; set; }
        public Nullable<System.DateTime> DateDeleted { get; set; }
        public Nullable<System.DateTime> DateDeactivated { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public string Mobile { get; set; }
        public string Passsword { get; set; }

    }

    //Employee Soft Delete
    public class EmployeeDelete
    {
        public long id { get; set; }

    }
    //Employee Hard Delete
    public class EmployeeeRemove
    {
        public long id { get; set; }
    }
}
