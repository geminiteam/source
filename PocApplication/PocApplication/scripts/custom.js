﻿$(document).ready(function () {

    var domain = $(location).attr('protocol')+'//' + $(location).attr('hostname') + ':' + $(location).attr('port');
    console.log(domain);

    if (localStorage.getItem('user')) {
        $('#private-page').css('display', 'block');
        $('#edit_profile').css('display', 'none');
        $('#user_directory').css('display', 'none');
        $('#view_profile').css('display', 'block');
        var user = JSON.parse(localStorage.getItem('user'));
        var print = '<p>First Name:&nbsp;' + user.FirstName + '</p>';
        print += '<p>LastName:&nbsp' + user.LastName + '</p>';
        print += '<p>Email:&nbsp;' + user.Email + '</p>';
        print += '<p>Password:&nbsp;' + user.Passsword + '</p>';
        print += '<p>DateofBirth&nbsp;' + user.DOB + '</p>';
        print += '<p>Account Created Date&nbsp;' + user.DateCreated + '</p>';
        print += '<p>Mobile&nbsp;' + user.Mobile + '</p>';
        $('#view_profile').html(print);
        print = null;
    } else {
               var page = $(location).attr('pathname')
        if (page== '/Index/Home') {
            window.location.href = domain;
        }       
        //console.log($(location).attr('pathname'));
        $('#private-page').css('display', 'none');
    }

    $(document).on('click', '#act1', function () {
     
        $('#login-form').css('display', 'none');
        $('#register-form').css('display', 'block');
       
    });

    $(document).on('click', '#act2', function () {
      
        $('#register-form').css('display', 'none');
        $('#login-form').css('display', 'block');
    });

    //Add employee
    $(document).on('click', '#register', function () {
        //json data format required

        //var emp = {
        //    "FirstName": "Sagar",
        //    "LastName": "Bhattacharya",
        //    "Email": "sagar13912@gmail.com",
        //    "Passsword": "sagar",
        //    "DOB": "1993-12-03",
        //    "Mobile":"8744096752"

        //};
        var emp = {
            "FirstName": $('#reg_fname').val()||null,
            "LastName": $('#reg_lname').val()||null,
            "Email": $('#reg_email').val()||null,
            "Passsword": $('#reg_password').val()||null,
            "DOB": $('#reg_dob').val()||null,
            "Mobile": $('#reg_mob').val()||null

        };
        console.log(emp);
      
        $.ajax({
            url: domain+"/api/Crud/AddEmployee",
            method: "POST",
            data: JSON.stringify(emp),           
            contentType: "application/json",
            success: function (data) {
                if (data) {
                    var html = '<div class="alert alert-success alert-dismissable fade in">';
                    html += '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                    html += '<strong>Congrations!</strong>You are Successfully Registered';
                    html += '</div>';
                    //alert('Ye We made it');
                    $('#status-message').html(html);
                    html = "";
                    $(':input').not(':button, :submit, :reset, :hidden').val('');

                } else {
                    var html = '<div class="alert alert-warning alert-dismissable fade in">';
                    html += '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                    html += '<strong>Oops!</strong>Their is an error';
                    html += '</div>';
                    //alert('Ye We made it');
                    $('#status-message').html(html);
                    html = "";
                }
                
                 console.log(data);
            },
            failure: function (data) {
                //alert('Sorry');
                var html = '<div class="alert alert-warning alert-dismissable fade in">';
                html += '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                html += '<strong>Oops!</strong>Their is an error';
                html += '</div>';
                //alert('Ye We made it');
                $('#status-message').html(html);
                html = "";
                console.log(data);
            },
            error: function (err) {
             
                console.log(err);
            }
        });
    });
    //List employee
    $(document).on('click', '#login', function () {

        var emp = {
            "Email": $('#login_email').val() || null,
            "Passsword": $('#login_password').val() || null,
        }
        //console.log(emp);
        $.ajax({
            url: domain + "/api/Crud/Login",
            method: "POST",
            data: JSON.stringify(emp),
            contentType: "application/json",
            success: function (data) {
                //console.log(data);
              
                if (data) {
                    var html = '<div class="alert alert-success alert-dismissable fade in">';
                    html += '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                    html += '<strong>Congrations!</strong>You are Successfully Logged In';
                    html += '</div>';
                    //alert('Ye We made it');

                    $('#login-status').html(html);
                    html = "";
                    $(':input').not(':button, :submit, :reset, :hidden').val('');              
                    localStorage.setItem('user', JSON.stringify(data));
                    //var local = localStorage.getItem('user');
                    window.location.href = domain+'/Index/Home/'

                } else {
                    var html = '<div class="alert alert-warning alert-dismissable fade in">';
                    html += '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                    html += '<strong>Oops!</strong>Their is an error';
                    html += '</div>';
                    //alert('Ye We made it');
                    $('#login-status').html(html);
                    html = "";
                }

                //console.log(data);
            },
            failure: function (data) {
                //alert('Sorry');
                var html = '<div class="alert alert-warning alert-dismissable fade in">';
                html += '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                html += '<strong>Oops!</strong>Their is an error';
                html += '</div>';
                //alert('Ye We made it');
                $('#login-status').html(html);
                html = "";
                console.log(data);
            },
            error: function (err) {

                console.log(err);
            }
        });


    });

    $(document).on('click', '#act3', function () {
        //alert('act3');
        $('#edit_profile').css('display', 'none');
        $('#user_directory').css('display', 'none');
        $('#view_profile').css('display', 'block');
        var user = JSON.parse(localStorage.getItem('user'));
        var print = '<p>First Name:&nbsp;'+user.FirstName+'</p>';
        print += '<p>LastName:&nbsp' + user.LastName + '</p>';
        print += '<p>Email:&nbsp;' + user.Email + '</p>';
        print += '<p>Password:&nbsp;' + user.Passsword + '</p>';
        print += '<p>DateofBirth&nbsp;' + user.DOB + '</p>';
        print += '<p>Account Created Date&nbsp;' + user.DateCreated + '</p>';
        print += '<p>Mobile&nbsp;' + user.Mobile + '</p>';
        $('#view_profile').html(print);
        print = null;    


    });
    $(document).on('click', '#act4', function () {
        //alert('Update Details');
        var user = JSON.parse(localStorage.getItem('user'));

        $('#user_directory').css('display', 'none');
        $('#view_profile').css('display', 'none');
        $('#user_search').css('display', 'none');
        $('#edit_profile').css('display', 'block');

        $('#upd_fname').val(user.FirstName);
        $('#upd_lname').val(user.LastName);
        $('#upd_email').val(user.Email);
        $('#upd_password').val(user.Passsword);       
        $('#upd_mob').val(user.Mobile);

    })
    //Update Employee
    $(document).on('click', '#update', function () {
        console.log('update');
        var emp = {
            "id":JSON.parse(localStorage.getItem('user')).id,
            "FirstName": $('#upd_fname').val() || null,
            "LastName": $('#upd_lname').val() || null,
            "Email": $('#upd_email').val() || null,
            "Passsword": $('#upd_password').val() || null,           
            "Mobile": $('#upd_mob').val() || null
        };
        console.log(emp);
        $.ajax({
            url: domain + "/api/Crud/ProfileUpdate",
            method: "POST",
            data: JSON.stringify(emp),
            contentType: "application/json",
            success: function (data) {
                if (data) {
                    var emp2 = { "id": JSON.parse(localStorage.getItem('user')).id };
                    $.ajax({                        
                        url: domain + '/api/Crud/MyProfile',
                        method: "POST",
                        data: JSON.stringify(emp2),
                        contentType: "application/json",
                        success: function (data) {
                            localStorage.setItem('user', JSON.stringify(data));
                            var user1 = JSON.parse(localStorage.getItem('user'));
                            $('#upd_fname').val(user1.FirstName);
                            $('#upd_lname').val(user1.LastName);
                            $('#upd_email').val(user1.Email);
                            $('#upd_password').val(user1.Passsword);
                            $('#upd_mob').val(user1.Mobile);
                            console.log(data);
                        },
                        failure: function (err) {
                            console.log(err);
                        }
                    });
                    var html = '<div class="alert alert-success alert-dismissable fade in">';
                    html += '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                    html += '<strong>Congrations!</strong>Details Updated';
                    html += '</div>';
                    //alert('Ye We made it');
                    $('#update-message').html(html);
                    html = "";
                    $(':input').not(':button, :submit, :reset, :hidden').val('');

                } else {
                    var html = '<div class="alert alert-warning alert-dismissable fade in">';
                    html += '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                    html += '<strong>Oops!</strong>Their is an error';
                    html += '</div>';
                    //alert('Ye We made it');
                    $('#update-message').html(html);
                    html = "";
                }

                console.log(data);
            },
            failure: function (data) {
                //alert('Sorry');
                var html = '<div class="alert alert-warning alert-dismissable fade in">';
                html += '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                html += '<strong>Oops!</strong>Their is an error';
                html += '</div>';
                //alert('Ye We made it');
                $('#update-message').html(html);
                html = "";
                console.log(data);
            },
            error: function (err) {

                console.log(err);
            }
        });


      
    });

    function EmployeeList() {
        $.ajax({
            url: domain + "/api/Crud/EmployeeList",
            method: "GET",
            success: function (data) {
                var html = "";
                html += '<div class ="table-responsive">';
                html += '<table class="table table-striped table-bordered">';
                html += '<tr>';
                html += '<td>FirstName</td>';
                html += '<td>LastName</td>';               
                html += '<td>Mobile</td>';
                html += '<td>Edit</td>';
                html += '<td>Delete</td>';
                html += '</tr>';

                $.each(data, function (index, value) {
                    console.log(value);
                    html += '<tr>';
                    html += '<td>' + value.FirstName + '</td>';
                    html += '<td>' + value.LastName + '</td>';
                    html += '<td>' + value.Mobile + '</td>';
                    html += '<td><button class="btn btn-default edit-user">Edit<input type="hidden" class="key" value="' + value.id+'"/>';
                    html += '<input type="hidden" class="edit-fname" value="' + value.FirstName + '">';
                    html += '<input type="hidden" class="edit-lname" value="' + value.LastName + '">';
                    html += '<input type="hidden" class="edit-mob" value="' + value.Mobile + '"/>';              
                    html += '</button></td>';
                    html += '<td><button class="btn btn-warning delete-user">Delete<input type="hidden" class="key" value="' + value.id + '">';
                    html += '</button></td>';
                    html += '</tr>';
                });
                html += '</table>';
                html += '</div>';
                $('#user_directory').html(html);
            },
            failure: function (data) {

            }

        });

    }

    $(document).on('click', '#act5', function () {
        $('#view_profile').css('display', 'none');
        $('#edit_profile').css('display', 'none');
        $('#user_search').css('display', 'none');
        $('#user_directory').css('display', 'block');
        EmployeeList();
       
        
    })
    //Soft Delete Employee
    $(document).on('click', '.delete-user', function () {
        //alert('hello');
        var softDel = $(this).find('.key').val();
        var emp = {
           "id":Number(softDel)
        };
        console.log(JSON.stringify(emp));
        
        $.ajax({
            url: domain + "/api/Crud/EmployeeDelete",
            method: "POST",
            data: JSON.stringify(emp),
            contentType:"application/json",
            success: function (data) {
                console.log(data);
                EmployeeList();
            },
            failure: function (err) {
                console.log(err);
            }
        });
      
    });
    //Logout
    $(document).on('click', '#logout', function () {
        localStorage.clear();
        window.location.href = domain;

    });

    function updateModalUser() {
      //
    }

    $(document).on('click', '.edit-user', function () {

        var id = $(this).find('.key').val();
        var fname = $(this).find('.edit-fname').val();
        var lname = $(this).find('.edit-lname').val();
        var mob = $(this).find('.edit-mob').val();
        //console.log(id, fname, lname, mob);
        
        
        var header = "Edit User";
        //var content = "This is my dynamic content";
        var content = '<div id="modal-update"></div>';
        content += '<div class="form-group">';
        content +='<label for="FirstName">First Name</label>';
        content += '<input type="text" class="form-control" id="userEditfname" aria-describedby="fname" value="' + fname + '"/>';
        content += '</div>';
        content += '<div class="form-group">'
        content += '<label for="LastName">First Name</label>';
        content += '<input type="text" class="form-control" id="userEditlname" aria-describedby="fname" value="' + lname + '"/>';
        content += '</div>';
        content += '<div class="form-group">'
        content += '<label for="Mobile">Mobile</label>';
        content += '<input type="text" class="form-control" id="userEditmob" aria-describedby="fname" value="' + mob + '"/>';
        content += '</div>';
        content += '<input type="hidden" value="' + id + '" id="userEditId"/>';
        //var emp = {
        //    "id": id,
        //    "FirstName": $('#userEditfname').val(),
        //    "LastName": $('#userEditlname').val(),
        //    "Mobile":$('#userEditmob').val()
        //}
       
        var strSubmitFunc = "updateModalUser()";
        var btnText = "Update User";
        doModal('idMyModal', header, content, strSubmitFunc, btnText);


    });

    $(document).on('click', '#submitModal', function () {

        var emp = {
            "id": $('#userEditId').val(),
            "FirstName": $('#userEditfname').val(),
            "LastName": $('#userEditlname').val(),
            "Mobile": $('#userEditmob').val()
           
        };
        $.ajax({
            url: domain + "/api/Crud/EmployeeUpdate",
            method: "POST",
            data: JSON.stringify(emp),
            contentType: "application/json",
            success: function (data) {
                if (data) {
                    EmployeeList();
                    var html = '<div class="alert alert-success alert-dismissable fade in">';
                    html += '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                    html += '<strong>Congrations!</strong>Details Updated';
                    html += '</div>';
                    $('#modal-update').html(html);
                } else {
                    var html = '<div class="alert alert-warning alert-dismissable fade in">';
                    html += '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                    html += '<strong>Sorry!</strong>Cannnot Update';
                    html += '</div>';
                    $('#modal-update').html(html);
                }

                
            },
            failure: function (err) {
                console.log(err);
            }
        });




       
        //console.log(emp);
    });

    function doModal(placementId, heading, formContent, strSubmitFunc, btnText)
    {
        html =  '<div id="modalWindow" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="confirm-modal" aria-hidden="true">';
        html += '<div class="modal-dialog">';
        html += '<div class="modal-content">';
        html += '<div class="modal-header">';
        html += '<a class="close" data-dismiss="modal">×</a>';
        html += '<h4>'+heading+'</h4>'
        html += '</div>';
        html += '<div class="modal-body">';
        html += formContent;
        html += '</div>';
        html += '<div class="modal-footer">';
        if (btnText!='') {
            html += '<span class="btn btn-success" id="submitModal">';
            html +=  btnText;
            html += '</span>';
        }
        html += '<span class="btn" data-dismiss="modal">';
        
        html += '</span>'; // close button
        html += '</div>';  // footer
        html += '</div>';  // content
        html += '</div>';  // dialog
        html += '</div>';  // modalWindow
        $("#"+placementId).html(html);
        $("#modalWindow").modal();
        $("#dynamicModal").modal('show');
    }

    

});
