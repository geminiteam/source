﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MT.Bll;
using MT.Bo;

namespace PocApplication.Controllers
{
    public class CrudController : ApiController
    {   [HttpPost]
        public bool AddEmployee(EmployeeBasicInfo emp)
        {
            return AdminBll.AddEmployee(emp);
        }
        [HttpGet]
        public List<EmployeeListInfo> EmployeeList()
        {
            return AdminBll.EmployeeList();
        }
        [HttpPost]
        public List<EmployeeListInfo> EmployeeSearch(EmployeeFilter emp)
        {
            return AdminBll.EmployeeSearch(emp);
        }
        [HttpPost]
        public bool ProfileUpdate(EmployeeProfileUpdate emp)
        {
            return AdminBll.ProfileUpdate(emp);
        }
        [HttpPost]
        public bool EmployeeUpdate(EmployeeUpdate emp)
        {
            return AdminBll.EmployeeUpdate(emp);
        }
        [HttpPost]
        public bool EmployeeDelete(EmployeeDelete emp)
        {
           
            return AdminBll.EmployeeDelete(emp);
        }
        [HttpGet]
        public bool EmployeeRemove(EmployeeeRemove emp)
        {
            return AdminBll.EmployeeRemove(emp);
        }
        [HttpPost]
        public EmployeeProfile MyProfile(EmployeeProfile emp)
        {
            return AdminBll.MyProfile(emp);
        }
        [HttpPost]
        public EmployeeProfile Login(Credential emp)
        {
            return AdminBll.Login(emp);

        }
    }
}