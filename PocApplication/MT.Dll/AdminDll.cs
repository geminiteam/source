﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MT.Modal;
using MT.Bo;

namespace MT.Dll
{
    public class AdminDll
    {
        //Add Employee
        public static bool AddEmployee(EmployeeBasicInfo emp)
        {
            try
            {
                var entities = new EmployeeEntities();
                var employee = new employee()
                {
                    FirstName = emp.FirstName,
                    LastName = emp.LastName,
                    Email = emp.Email,
                    Passsword = emp.Passsword,
                    DOB = emp.DOB,
                    DateCreated = DateTime.UtcNow,
                    Mobile = emp.Mobile,
                    IsActive =true,
                    IsDeleted =false
                };
                entities.employees.Add(employee);
                entities.SaveChanges();

                return true;
            }
            catch (Exception es)
            {
                return false;
            }
        }
        //List Employee
        public static List<EmployeeListInfo> EmployeeList()
        {

            var entities = new EmployeeEntities();
            var context = entities.employees.Where(a => a.IsActive == true).Select(o => new EmployeeListInfo()
            {
                id = o.id,
                FirstName = o.FirstName,
                LastName = o.LastName,
                Email = o.Email,
                Mobile = o.Mobile,
                DateCreated = o.DateCreated,
                DateDeactivated = o.DateDeactivated,
                DateDeleted = o.DateDeleted

            }).ToList();
            return context;

        }
        public static EmployeeProfile Login(Credential emp)
        {
            var entities = new EmployeeEntities();
            return entities.employees.Where(a => a.Email == emp.Email && a.Passsword == emp.Passsword).Select(
                o => new EmployeeProfile()
                {
                    id = o.id,
                    FirstName = o.FirstName,
                    LastName = o.LastName,
                    Email = o.Email,
                    DOB = o.DOB,
                    DateDeleted = o.DateDeleted,
                    DateCreated = o.DateCreated,
                    DateDeactivated = o.DateDeactivated,
                    Passsword = o.Passsword,
                    IsActive = o.IsActive,
                    IsDeleted = o.IsDeleted,
                    Mobile = o.Mobile
                }
                ).FirstOrDefault();
        }
        public static EmployeeProfile MyProfile(EmployeeProfile emp)
        {
            var entities = new EmployeeEntities();
            return entities.employees.Where(a => a.id == emp.id).Select(o => new EmployeeProfile() { 
            id = o.id,
            FirstName = o.FirstName,
            LastName = o.LastName,
            Email = o.Email,
            DOB = o.DOB,
            DateDeleted = o.DateDeleted,
            DateCreated = o.DateCreated,
            DateDeactivated = o.DateDeactivated,
            Passsword = o.Passsword,
            IsActive = o.IsActive,
            IsDeleted = o.IsDeleted,
            Mobile = o.Mobile           
            
            }).FirstOrDefault();
         
        }
        //Search Employee
        public static List<EmployeeListInfo> EmployeeSearch(EmployeeFilter emp)
        {
            var entities = new EmployeeEntities();
            var context = entities.employees.Where(a => a.FirstName.Contains(emp.FirstName) || a.LastName.Contains(emp.LastName) || a.Email.Contains(emp.Email))
                          .Select(o => new EmployeeListInfo()
                          {
                              id = o.id,
                              FirstName = o.FirstName,
                              LastName = o.LastName,
                              Email = o.Email,
                              Mobile = o.Mobile,
                              DateCreated = o.DateCreated,
                              DateDeactivated = o.DateDeactivated,
                              DateDeleted = o.DateDeleted
                          }).ToList();
            
            return context;

        }
        //Update Profile
        public static bool ProfileUpdate(EmployeeProfileUpdate emp){
            var entities = new EmployeeEntities();
            var employee = entities.employees.Where(a => a.id == emp.id).FirstOrDefault();
            if (employee != null)
            {
                employee.FirstName = emp.FirstName;
                employee.LastName = emp.LastName;
                employee.Email = emp.Email;
                employee.DOB = emp.DOB;
                employee.Passsword = emp.Passsword;
                employee.Mobile = emp.Mobile;
                entities.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
            
        }

        //Update Employee
        public static bool EmployeeUpdate(EmployeeUpdate emp)
        {
            var entities = new EmployeeEntities();
            var employee = entities.employees.Where(a => a.id == emp.id).FirstOrDefault();
            if (employee != null)
            {
                employee.FirstName = emp.FirstName;
                employee.LastName = emp.LastName;             
                employee.Mobile = emp.Mobile;
                entities.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }

        }
        //Soft Delete Profile
        public static bool EmployeeDelete(EmployeeDelete emp)
        {
            var entities = new EmployeeEntities();
            var employee = entities.employees.Where(a => a.id == emp.id).FirstOrDefault();
            if (employee != null)
            {
                employee.IsActive = false;
                employee.IsDeleted = true;
                entities.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }

        }
        //Hard Delete Profile
        public static bool EmployeeRemove(EmployeeeRemove emp)
        {
            var entities = new EmployeeEntities();
            var employee = entities.employees.Where(a => a.id == emp.id).FirstOrDefault();
            if (employee != null)
            {
                entities.employees.Remove(employee);
                return true;
            }
            else
            {
                return false;
            }

        }

    }
}
