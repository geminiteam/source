﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LA.BO
{
    public class EmployeeBO
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string mobile { get; set; }
        public int designation { get; set; }
        public DateTime dob { get; set; }
    }

    public class EmployeeLogin
    {
        public string email {get; set; }
        public string password { get; set; }
    }
}
